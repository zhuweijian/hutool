package com.xiaoleilu.hutool.log;

/**
 * 日志等级
 * @author Looly
 *
 */
public enum LogLevel {
	/**
	 * 'TRACE' log level.
	 */
	TRACE,
	/**
	 * 'DEBUG' log level.
	 */
	DEBUG,
	/**
	 * 'INFO' log level.
	 */
	INFO,
	/**
	 * 'WARN' log level.
	 */
	WARN,
	/**
	 * 'ERROR' log level.
	 */
	ERROR
}
